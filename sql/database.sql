set foreign_key_checks=0;
#SQLDELIMETER
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(1023) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#SQLDELIMETER
CREATE TABLE `tags` (
  `file` int(10) unsigned NOT NULL,
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`file`,`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#SQLDELIMETER
CREATE TABLE `tags_cache` (
  `tag` varchar(255) NOT NULL,
  `files` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#SQLDELIMETER
set foreign_key_checks=1;