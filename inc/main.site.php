<?php
ob_start();
session_start();
include "main.php";

function genLink($s){
global $config;
return $config['url_home']."/".$s;
}
function getURL($type, $id){
global $config;
switch($type){
case "thumb":
$url=$config['link_thumbnail'];
break;
case "svg":
$url=$config['link_svg'];
break;
case "info":
$url=$config['link_info'];
break;
}

return str_replace("%id%",$id,$url);
}


function filelistitem($v){
/*return "<li><b>#".$v['id']."</b> ".$v['name']." <br>[ 
<a href=\"".getURL('thumb',$v['id'])."\">PNG</a> | 
<a href=\"".getURL('svg',$v['id'])."\">SVG</a> | 
<a href=\"".getURL('info',$v['id'])."\">INFO</a> ]";
*/
if(!$v['name']){
$v['name']="Unknown";
}

return "<li><b>#".$v['id']."</b> <a href=\"/ocal/file/".$v['id']."\">".$v['name']."</a>";
}

function pagination($count,$pagelen,$page=0){
$pages=floor($count/$pagelen);
if($pages*$pagelen!=$count){
$pages++;
}
if($page==0){
$page=$pages;
}
if($page<$pages){
$rows=$pagelen;
}else{
$rows=$count-($pages-1)*$pagelen;
}
//echo "CNT:$count Pages: $pages Pagelen: $pagelen Page: $page ROWS: $rows";
return array('pages'=>$pages,'page'=>$page,'rows'=>$rows);
}
